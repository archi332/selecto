<?php

namespace AppBundle\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class UserRepository extends EntityRepository
{
    const TABLE_USER_ALIAS = 'user';
    const TABLE_USER_CONFIGURATION_ALIAS = 'config';

    /**
     * @var array
     */
    private $orderDirections = [
        Criteria::ASC,
        Criteria::DESC,
    ];

    /**
     * @var array
     */
    private $orderColumns = [
        'id',
        'email',
    ];

    /**
     * @var array
     */
    private $filterColumns = [
        'id' => self::TABLE_USER_ALIAS,
        'email' => self::TABLE_USER_ALIAS,
        'name' => self::TABLE_USER_CONFIGURATION_ALIAS,
    ];

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findOneUserWithConfiguration(array $criteria = [])
    {
        try {
            return $this->queryUsersWithConfigurationFiltered($criteria, '', '', 1)
                ->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }
    }

    /**
     * @return array
     */
    public function findAllWithConfiguration()
    {
        return $this->queryUsersWithConfigurationFiltered([], '', '')->getArrayResult();
    }

    /**
     * @param array $criteria
     * @param string $orderColumn
     * @param string $orderDirection
     *
     * @return array
     */
    public function findUsersWithConfigurationFiltered(array $criteria = [], string $orderColumn, string $orderDirection)
    {
        return $this->queryUsersWithConfigurationFiltered($criteria, $orderColumn, $orderDirection)->getArrayResult();
    }

    /**
     * @param array $criteria
     * @param string $orderColumn
     * @param string $orderDirection
     * @param int $limit
     *
     * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
     */
    private function queryUsersWithConfigurationFiltered(
        array $criteria = [],
        string $orderColumn,
        string $orderDirection,
        int $limit = null
    )
    {
        $qb = $this->createQueryBuilder('user');

        $query = $qb
            ->select(['user.id', 'user.email', 'user.role', 'config.phone, config.name, config.lastName'])
            ->innerJoin('user.configuration', 'config');

        if (
            array_search(strtoupper($orderDirection), $this->orderDirections) !== false
            && array_search(strtolower($orderColumn), $this->orderColumns) !== false
        ) {
            $query = $query->orderBy(sprintf('user.%s', strtolower($orderColumn)), $orderDirection);
        }

        if ($limit) {
            $query = $query->setMaxResults($limit);
        }

        foreach ($criteria as $column => $value) {
            if (array_key_exists($column, $this->filterColumns) && $value) {

                $columnAlias = $this->filterColumns[$column] . '.' . $column;
                $query = $query
                    ->andWhere($qb->expr()->like($columnAlias, ':param' . $column))
                    ->setParameter('param' . $column, '%' . $value . '%');

            }
        }

        return $query->getQuery();
    }

}
