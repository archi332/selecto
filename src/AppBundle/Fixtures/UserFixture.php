<?php

namespace AppBundle\Fixtures;

use AppBundle\Entity\User;
use AppBundle\Entity\UserConfiguration;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixture extends Fixture
{
    const PASSWORD_TEST = '$2y$13$aOUdtkVVbOYGfe7dHtrxuu6Ex1Rmi8P.75E6hwz960cInfkG5e3OG';
    const PASSWORD_USER = '$2y$13$Q3PgxeRMAVocA96B4gilGOEKBQbr2ClmUqzt2ZtEBcsDLCymUpb5W';
    const PASSWORD_ADMIN = '$2y$13$CkPuhYf9V.HaBt5I17c5X.4yD9GltTzO1aznkWOkpZjRGXV1Yg4te';

    const SAMPLE_FIRST_NAME = 'some name #';
    const SAMPLE_LAST_NAME = 'some Last name #';

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $this->addUser(
            '',
            self::PASSWORD_USER,
            'User',
            'User Last Name',
            User::USER_ROLE
        );

        $this->addUser(
            '',
            self::PASSWORD_ADMIN,
            'Admin',
            'Admin Last Name',
            User::ADMIN_ROLE
        );


        for ($i = 0; $i < 20; $i++) {
            $this->addUser($i, self::PASSWORD_TEST, rand(999, 9999999));
        }

        $manager->flush();

    }

    /**
     * @param string $prefix
     * @param string $password
     * @param string $name
     * @param string $lastName
     * @param string $role
     */
    private function addUser(
        $prefix = '',
        $password = self::PASSWORD_TEST,
        $name = self::SAMPLE_FIRST_NAME,
        $lastName = self::SAMPLE_LAST_NAME,
        $role = User::USER_ROLE
    )
    {

        $user = new User();
        $user->setEmail($prefix . $name . '@' . 'mail.com');
        $user->setPassword($password);
        $user->setRole($role);

        $userConfig = new UserConfiguration();
        $userConfig->setName($name . $prefix);
        $userConfig->setLastName($lastName . $prefix);
        $userConfig->setPhone($prefix . rand(999, 999999));

        $user->setConfiguration($userConfig);
        $this->manager->persist($userConfig);
        $this->manager->persist($user);
    }
}