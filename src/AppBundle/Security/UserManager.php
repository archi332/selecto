<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use AppBundle\Entity\UserConfiguration;
use AppBundle\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserManager
{
    /**
     * @var ObjectManager
     */
    private $om;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var array
     */
    private $roleArray = [
        'admin' => User::ADMIN_ROLE,
        'user' => User::USER_ROLE,
    ];

    /**
     * @param ObjectManager $objectManager
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(ObjectManager $objectManager, UserPasswordEncoderInterface $encoder)
    {
        $this->om = $objectManager;
        $this->encoder = $encoder;
    }

    /**
     * @return UserRepository|ObjectRepository
     */
    protected function getRepository()
    {
        return $this->om->getRepository(User::class);
    }

    /**
     * @param string $email
     * @return null|object
     */
    public function findUserByEmail($email)
    {
        return $this->getRepository()->findOneBy(['email' => $email]);
    }

    /**
     * @param string $email
     * @return null|object
     */
    public function findUserByUsername($email)
    {
        return $this->getRepository()->findOneBy(['email' => $email]);
    }

    /**
     * @param UserInterface $user
     */
    public function deleteUser(UserInterface $user)
    {
        $this->om->remove($user);
        $this->om->flush();
    }

    /**
     * @param array $criteria
     *
     * @return User|object
     */
    public function findUserBy(array $criteria)
    {
        return $this->getRepository()->findOneUserWithConfiguration($criteria);
    }

    /**
     * @param array $criteria
     * @param string $orderColumn
     * @param string $orderDirection
     *
     * @return array
     */
    public function findUsersBy(array $criteria, string $orderColumn, string $orderDirection)
    {
        return $this->getRepository()->findUsersWithConfigurationFiltered($criteria, $orderColumn, $orderDirection);
    }

    /**
     * @return array
     */
    public function findUsers()
    {
        return $this->getRepository()->findAllWithConfiguration();
    }

    public function reloadUser(UserInterface $user)
    {
    }

    /**
     * @param ParameterBag $fields
     *
     * @return array
     */
    public function createUserFromParameterBag(ParameterBag $fields): array
    {
        try {
            $user = new User();
            $user->setEmail($fields->get('email'));
            $user->setPassword($this->encoder->encodePassword($user, $fields->get('password')));
            $user->setRole(
                $this->roleArray[$fields->get('role')] ?? $this->roleArray['user']
            );

            $configuration = new UserConfiguration();
            $configuration->setPhone($fields->get('phone'));
            $configuration->setName($fields->get('name'));
            $configuration->setLastName($fields->get('lastName'));

            $user->setConfiguration($configuration);

            $this->om->persist($user);
            $this->om->persist($configuration);

            $this->om->flush();

            $message = sprintf('User \'%s\' was created. Id = %s', $user->getEmail(), $user->getId());

        } catch (\Exception $e) {
            //@TODO: Must be some like Sentry error - not display for user
            $success = false;
            $message = $e->getMessage();
        }

        return [
            'success' => $success ?? true,
            'message' => $message,
        ];
    }

    /**
     * @param User $user
     * @param ParameterBag $fields
     * @param bool $userSelfUpdate
     *
     * @return array
     */
    public function updateUser(User $user, ParameterBag $fields, bool $userSelfUpdate): array
    {
        try {

            if ($fields->get('email')) {
                $user->setEmail($fields->get('email'));
            }

            if ($fields->get('password')) {
                $user->setPassword($this->encoder->encodePassword($user, $fields->get('password')));
            }

            if ($fields->get('email')) {
                $user->setEmail($fields->get('email'));
            }

            if (!$userSelfUpdate) {
                if ($fields->get('role')) {

                    $user->setRole(
                        $this->roleArray[$fields->get('role')] ?? $this->roleArray['user']
                    );
                }
            }

            $configuration = $user->getConfiguration();

            if ($fields->get('phone')) {
                $configuration->setPhone($fields->get('phone'));
            }

            if ($fields->get('name')) {
                $configuration->setName($fields->get('name'));
            }

            if ($fields->get('lastName')) {
                $configuration->setLastName($fields->get('lastName'));
            }

            $user->setConfiguration($configuration);

            $this->om->persist($user);

            $this->om->flush();

            $message = sprintf('User \'%s\' was updated. Id = %s', $user->getEmail(), $user->getId());

        } catch (\Exception $e) {
            //@TODO: Must be some like Sentry error - not display for user
            $success = false;
            $message = $e->getMessage();
        }

        return [
            'success' => $success ?? true,
            'message' => $message,
        ];
    }
}
