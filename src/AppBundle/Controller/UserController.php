<?php

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use AppBundle\Security\UserManager;
use Doctrine\Common\Collections\Criteria;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @method User getUser()
 * @package AppBundle\Controller
 */
class UserController extends FOSRestController
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @param UserManager $manager
     */
    public function __construct(UserManager $manager)
    {
        $this->userManager = $manager;
    }

    /**
     * @Rest\Get("/api/user")
     *
     * @param Request $request
     *
     * @return array|View
     */
    public function getAction(Request $request)
    {
        $role = $this->getUser()->getRole();

        $criteria['name'] = $request->get('name', null);
        $criteria['email'] = $request->get('email', null);

        if ($role === User::USER_ROLE) {
            $criteria['role'] = User::USER_ROLE;
        }

        $restResult = $this->userManager->findUsersBy(
            $criteria,
            $request->get('sort', 'id'),
            $request->get('order', Criteria::ASC)
        );

        if ($restResult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return $restResult;
    }

    /**
     * @Rest\Get("/api/user/{id}")
     *
     * @param int $id
     * @return null|object
     */
    public function showAction(int $id)
    {
        $user = $this->userManager->findUserBy(['id' => $id]);

        if ($this->getUser()->hasRole(User::USER_ROLE) && $user['role'] === User::ADMIN_ROLE) {

            throw new AccessDeniedHttpException('You are not allowed to show this user');

        } elseif (!$user) {

            throw new NotFoundHttpException('User not found');
        }

        return $user;
    }

    /**
     * @Rest\Post("/api/user")
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        return $this->userManager->createUserFromParameterBag($request->request);
    }

    /**
     * @Rest\Put("/api/user/{id}")
     *
     * @param Request $request
     * @param int $id
     *
     * @return array
     */
    public function updateAction(Request $request, int $id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $userSelfUpdate = ($this->getUser()->hasRole(User::USER_ROLE) && $this->getUser()->getId() === $id);

        if (!$user) {
            throw new NotFoundHttpException('User not found');

        } elseif ($this->getUser()->hasRole(User::USER_ROLE) && !$userSelfUpdate) {

            throw new AccessDeniedHttpException('You are not allowed to update this user');
        }



        return $this->userManager->updateUser($user, $request->request, $userSelfUpdate);
    }

    /**
     * @Rest\Delete("/api/user/{id}")
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param int $id
     *
     * @return array
     */
    public function deleteAction(int $id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if (!$user) {
            throw new NotFoundHttpException('User not found');

        } elseif ($user->hasRole(User::ADMIN_ROLE)) {

            throw new AccessDeniedHttpException('You are not allowed to delete user with role \'Admin\'');

        } elseif ($user->getId() === $id) {

            throw new BadRequestHttpException('You can\'t delete yourself');

        } else {

            $this->userManager->deleteUser($user);
        }

        return ['success' => true];
    }
}